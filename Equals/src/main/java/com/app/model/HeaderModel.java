package com.app.model;

import java.sql.Date;

public class HeaderModel {
    public HeaderModel(){

    }

    private Date DataInicioPeriodo;
    private Date DataFimPeriodo;
    private Long NumeroEstabelecimento;
    private Date DataProcessamento;
    private Long Sequencial;
    private String NomeAdquirente;
    private Long TipoExtrato;

    public Date getDataInicioPeriodo() {
        return DataInicioPeriodo;
    }

    public void setDataInicioPeriodo(Date dataInicioPeriodo) {
        DataInicioPeriodo = dataInicioPeriodo;
    }

    public Date getDataFimPeriodo() {
        return DataFimPeriodo;
    }

    public void setDataFimPeriodo(Date dataFimPeriodo) {
        DataFimPeriodo = dataFimPeriodo;
    }

    public Long getNumeroEstabelecimento() {
        return NumeroEstabelecimento;
    }

    public void setNumeroEstabelecimento(Long numeroEstabelecimento) {
        NumeroEstabelecimento = numeroEstabelecimento;
    }
    public Date getDataProcessamento() {
        return DataProcessamento;
    }

    public void setDataProcessamento(Date dataProcessamento) {
        DataProcessamento = dataProcessamento;
    }

    public Long getSequencial() {
        return Sequencial;
    }

    public void setSequencial(Long sequencial) {
        Sequencial = sequencial;
    }

    public String getNomeAdquirente() {
        return NomeAdquirente;
    }

    public void setNomeAdquirente(String nomeAdquirente) {
        NomeAdquirente = nomeAdquirente;
    }

    public Long getTipoExtrato() {
        return TipoExtrato;
    }

    public void setTipoExtrato(Long tipoExtrato) {
        TipoExtrato = tipoExtrato;
    }
}
