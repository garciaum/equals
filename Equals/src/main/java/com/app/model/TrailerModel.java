package com.app.model;

public class TrailerModel {
    public TrailerModel() {

    }

    private Long TotalRegistros;
    private Integer IdHeader;
    public Long getTotalRegistros() {
        return TotalRegistros;
    }

    public void setTotalRegistros(Long totalRegistros) {
        TotalRegistros = totalRegistros;
    }

    public Integer getIdHeader() {
        return IdHeader;
    }

    public void setIdHeader(Integer idHeader) {
        IdHeader = idHeader;
    }
}
