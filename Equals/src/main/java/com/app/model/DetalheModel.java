package com.app.model;

import java.sql.Date;
import java.sql.Time;

public class DetalheModel {
    public DetalheModel() {

    }

    private Long CodigoEstabelecimento;
    private Integer IdHeader;
    private Date DataTransacao;
    private Date DataEvento;
    private Time HoraEvento;
    private Integer TipoEvento;
    private Integer TipoTransacao;
    private String CodigoTransacao;
    private Double ValorTransacao;
    private Double ValorParcelaLiquido;
    private String Pagamento;
    private Integer QtdParcelas;
    private Date DataPrevistaPagamento;
    private Double TaxaParcelamento;
    private Double TaxaIntermediacao;
    private Double TarifaIntermediacao;
    private Double TarifaBoleto;
    private Double RepasseAplicacao;
    private Double ValorLiquidoTransacao;
    private Integer StatusPagamento;
    private String NomeInstituicaoBandeira;
    private String CodigoPedido;
    private String CartaoBin;
    private String CartaoHolder;
    private String CodigoNsu;

    public Long getCodigoEstabelecimento() {
        return CodigoEstabelecimento;
    }

    public void setCodigoEstabelecimento(Long codigoEstabelecimento) {
        CodigoEstabelecimento = codigoEstabelecimento;
    }

    public Date getDataTransacao() {
        return DataTransacao;
    }

    public void setDataTransacao(Date dataTransacao) {
        DataTransacao = dataTransacao;
    }

    public Date getDataEvento() {
        return DataEvento;
    }

    public void setDataEvento(Date dataEvento) {
        DataEvento = dataEvento;
    }

    public Time getHoraEvento() {
        return HoraEvento;
    }

    public void setHoraEvento(Time horaEvento) {
        HoraEvento = horaEvento;
    }

    public Integer getTipoEvento() {
        return TipoEvento;
    }

    public void setTipoEvento(Integer tipoEvento) {
        TipoEvento = tipoEvento;
    }

    public Integer getTipoTransacao() {
        return TipoTransacao;
    }

    public void setTipoTransacao(Integer tipoTransacao) {
        TipoTransacao = tipoTransacao;
    }

    public String getCodigoTransacao() {
        return CodigoTransacao;
    }

    public void setCodigoTransacao(String codigoTransacao) {
        CodigoTransacao = codigoTransacao;
    }

    public Double getValorTransacao() {
        return ValorTransacao;
    }

    public void setValorTransacao(Double valorTransacao) {
        ValorTransacao = valorTransacao;
    }

    public Double getValorParcelaLiquido() {
        return ValorParcelaLiquido;
    }

    public void setValorParcelaLiquido(Double valorParcelaLiquido) {
        ValorParcelaLiquido = valorParcelaLiquido;
    }

    public String getPagamento() { return Pagamento; }

    public void setPagamento(String pagamento) {
        Pagamento = pagamento;
    }

    public Integer getQtdParcelas() {
        return QtdParcelas;
    }

    public void setQtdParcelas(Integer qtdParcelas) {
        QtdParcelas = qtdParcelas;
    }

    public Date getDataPrevistaPagamento() {
        return DataPrevistaPagamento;
    }

    public void setDataPrevistaPagamento(Date dataPrevistaPagamento) {
        DataPrevistaPagamento = dataPrevistaPagamento;
    }

    public Double getTaxaParcelamento() {
        return TaxaParcelamento;
    }

    public void setTaxaParcelamento(Double taxaParcelamento) {
        TaxaParcelamento = taxaParcelamento;
    }

    public Double getTaxaIntermediacao() {
        return TaxaIntermediacao;
    }

    public void setTaxaIntermediacao(Double taxaIntermediacao) {
        TaxaIntermediacao = taxaIntermediacao;
    }

    public Double getTarifaIntermediacao() {
        return TarifaIntermediacao;
    }

    public void setTarifaIntermediacao(Double tarifaIntermediacao) {
        TarifaIntermediacao = tarifaIntermediacao;
    }

    public Double getTarifaBoleto() {
        return TarifaBoleto;
    }

    public void setTarifaBoleto(Double tarifaBoleto) {
        TarifaBoleto = tarifaBoleto;
    }

    public Double getRepasseAplicacao() {
        return RepasseAplicacao;
    }

    public void setRepasseAplicacao(Double repasseAplicacao) {
        RepasseAplicacao = repasseAplicacao;
    }

    public Double getValorLiquidoTransacao() {
        return ValorLiquidoTransacao;
    }

    public void setValorLiquidoTransacao(Double valorLiquidoTransacao) {
        ValorLiquidoTransacao = valorLiquidoTransacao;
    }

    public Integer getStatusPagamento() {
        return StatusPagamento;
    }

    public void setStatusPagamento(Integer statusPagamento) {
        StatusPagamento = statusPagamento;
    }

    public String getNomeInstituicaoBandeira() {
        return NomeInstituicaoBandeira;
    }

    public void setNomeInstituicaoBandeira(String nomeInstituicaoBandeira) {
        NomeInstituicaoBandeira = nomeInstituicaoBandeira;
    }

    public Integer getIdHeader() {
        return IdHeader;
    }

    public void setIdHeader(Integer idHeader) {
        IdHeader = idHeader;
    }

    public String getCodigoPedido() {
        return CodigoPedido;
    }

    public void setCodigoPedido(String codigoPedido) {
        CodigoPedido = codigoPedido;
    }

    public String getCartaoBin() {
        return CartaoBin;
    }

    public void setCartaoBin(String cartaoBin) {
        CartaoBin = cartaoBin;
    }

    public String getCartaoHolder() {
        return CartaoHolder;
    }

    public void setCartaoHolder(String cartaoHolder) {
        CartaoHolder = cartaoHolder;
    }

    public String getCodigoNsu() {
        return CodigoNsu;
    }

    public void setCodigoNsu(String codigoNsu) {
        CodigoNsu = codigoNsu;
    }
}
