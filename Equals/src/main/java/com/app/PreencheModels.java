package com.app;

import com.app.model.DetalheModel;
import com.app.model.HeaderModel;
import com.app.model.TrailerModel;

import static com.app.utils.Extensions.*;
import static com.app.utils.Extensions.stringParaLong;

public class PreencheModels {
    public static HeaderModel preencheHeader(String line) {
        HeaderModel header = new HeaderModel();
        header.setNumeroEstabelecimento(stringParaLong(line.substring(1,11)));
        header.setDataProcessamento(stringParaData(line.substring(11,19)));
        header.setDataInicioPeriodo(stringParaData(line.substring(19,27)));
        header.setDataFimPeriodo(stringParaData(line.substring(27,35)));
        header.setSequencial(stringParaLong(line.substring(35,42)));
        header.setTipoExtrato(stringParaLong(line.substring(47,48)));
        header.setNomeAdquirente(line.substring(42,47));
        return header;
    }
    public static DetalheModel preencheDetalhe(String line) {
        DetalheModel detalhe = new DetalheModel();
        detalhe.setCodigoEstabelecimento(stringParaLong(line.substring(1,11)));
        detalhe.setDataTransacao(stringParaData(line.substring(11,19)));
        detalhe.setDataEvento(stringParaData(line.substring(19,27)));
        detalhe.setHoraEvento(stringParaHora(line.substring(27,33)));
        detalhe.setTipoEvento(stringParaInteger(line.substring(33,35)));
        detalhe.setTipoTransacao(stringParaInteger(line.substring(35,37)));
        detalhe.setCodigoTransacao(line.substring(45,77));
        detalhe.setCodigoPedido(line.substring(77,97));
        detalhe.setValorTransacao(stringParaDouble(line.substring(97,110),2));
        detalhe.setValorParcelaLiquido(stringParaDouble(line.substring(110,123),2));
        detalhe.setPagamento(line.substring(123,124));
        detalhe.setQtdParcelas(stringParaInteger(line.substring(128,130)));
        detalhe.setDataPrevistaPagamento(stringParaData(line.substring(130,138)));
        detalhe.setTaxaParcelamento(stringParaDouble(line.substring(177,190),2));
        detalhe.setTaxaIntermediacao(stringParaDouble(line.substring(190,203),2));
        detalhe.setTarifaIntermediacao(stringParaDouble(line.substring(203,216),2));
        detalhe.setTarifaBoleto(stringParaDouble(line.substring(216,229),2));
        detalhe.setRepasseAplicacao(stringParaDouble(line.substring(229,242),2));
        detalhe.setValorLiquidoTransacao(stringParaDouble(line.substring(242,255),2));
        detalhe.setStatusPagamento(stringParaInteger(line.substring(255,257)));
        detalhe.setNomeInstituicaoBandeira(line.substring(261,291));
        detalhe.setCodigoNsu(line.substring(329,340));
        detalhe.setCartaoBin(line.substring(343,349));
        detalhe.setCartaoHolder(line.substring(349,353));

        return detalhe;
    }
    public static TrailerModel preencheTrailer(String line) {
        TrailerModel trailer = new TrailerModel();
        trailer.setTotalRegistros(stringParaLong(line.substring(1,12)));

        return trailer;
    }
}
