package com.app;

import com.app.dbconfig.MysqlConnect;
import com.app.model.LayoutArquivo;

import java.io.*;
import java.sql.*;
import java.util.ArrayList;

import static com.app.querySql.InsertDados.*;
import static com.app.PreencheModels.*;

public class ManipulaArquivo {
    public static void main(String[] args) {
        LeArquivo();
    }

    public static void LeArquivo() {
        String directory = "src/main/resources";
        String fileName = "processoSeletivoEquals.txt";
        String absolutePath = directory + File.separator + fileName;
        LayoutArquivo layoutArquivo = new LayoutArquivo();
        try(BufferedReader bufferedReader = new BufferedReader(new FileReader(absolutePath))) {
            String line = bufferedReader.readLine();
            layoutArquivo.Detalhes = new ArrayList<>();
            while(line != null) {
                switch (line.charAt(0)){
                    case '0':
                        layoutArquivo.Header = preencheHeader(line);
                        break;
                    case '1':
                        layoutArquivo.Detalhes.add(preencheDetalhe(line));
                        break;
                    case '9':
                        layoutArquivo.Trailer = preencheTrailer(line);
                        break;
                    default:
                        break;
                }
                line = bufferedReader.readLine();
            }
            salvaArquivo(layoutArquivo);
        } catch (FileNotFoundException fe) {
            fe.printStackTrace();
        } catch (IOException ioe) {
            ioe.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private static void salvaArquivo(LayoutArquivo layoutArquivo) {
        MysqlConnect mysqlConnect = new MysqlConnect();
        try {
            Connection connection = mysqlConnect.connect();
            String sql = "SELECT Nu_Estabelecimento FROM tb_header where Nu_Sequencial = ?";
            PreparedStatement statement = connection.prepareStatement(sql);
            statement.setLong(1, layoutArquivo.Header.getSequencial());
            ResultSet result = statement.executeQuery();
            if (!result.first()) {
                Integer idHeader = salvarHeader(connection, layoutArquivo.Header);
                Integer idTrailer = salvarTrailer(connection, layoutArquivo.Trailer);
                salvarDetalhe(connection, layoutArquivo.Detalhes, idHeader, idTrailer);
            }
            statement.close();
        } catch (SQLException se) {
            se.printStackTrace();
        } finally {
            mysqlConnect.disconnect();
        }
    }
}
