package com.app.servlets;

import com.app.ManipulaArquivo;
import com.app.model.LayoutArquivo;
import com.app.querySql.PesquisaBanco;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(
    name = "equalsservlet",
    urlPatterns = "/EqualsAdquirente"
)

public class EqualsServlet extends HttpServlet {

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        ManipulaArquivo.LeArquivo();
        LayoutArquivo layout = PesquisaBanco.ObterDados(null, null, null, null, null, null);

        req.setAttribute("Header", layout.Header);
        req.setAttribute("Detalhes", layout.Detalhes);
        req.setAttribute("Trailer", layout.Trailer);
        req.setCharacterEncoding("UTF-8");
        RequestDispatcher view = req.getRequestDispatcher("index.jsp");
        view.forward(req, resp);
    }
}
