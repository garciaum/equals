package com.app.servlets;

import com.app.querySql.PesquisaBanco;
import com.app.model.LayoutArquivo;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.Date;

import static com.app.utils.Extensions.stringParaData;
import static com.app.utils.Extensions.stringParaLong;

@WebServlet(
        name = "equalspesquisa",
        urlPatterns = "/EqualsPesquisa"
)

public class PesquisaServlet extends HttpServlet {

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        if(req != null) {
            Date dataInicio = null;
            Date dataFim = null;
            Long numeroEstabelecimento = null;
            String codigoNsu = null;
            String cartaoBin = null;
            String cartaoHolder = null;
            if (req.getParameter("dataInicio") != null)
                dataInicio = stringParaData(req.getParameter("dataInicio"));
            if (req.getParameter("dataFim") != null)
                dataFim = stringParaData(req.getParameter("dataFim"));
            if (req.getParameter("numeroEstabelecimento") != null)
                numeroEstabelecimento = stringParaLong(req.getParameter("numeroEstabelecimento"));
            if (req.getParameter("codigoNsu") != null)
                codigoNsu = req.getParameter("codigoNsu");
            if (req.getParameter("cartaoBin") != null)
                cartaoBin = req.getParameter("cartaoBin");
            if (req.getParameter("cartaoHolder") != null)
                cartaoHolder = req.getParameter("cartaoHolder");
            LayoutArquivo layout = PesquisaBanco.ObterDados(numeroEstabelecimento, dataInicio, dataFim, codigoNsu, cartaoBin, cartaoHolder);

            req.setAttribute("Header", layout.Header);
            req.setAttribute("Detalhes", layout.Detalhes);
            req.setAttribute("Trailer", layout.Trailer);
            req.setCharacterEncoding("UTF-8");
            RequestDispatcher view = req.getRequestDispatcher("index.jsp");
            view.forward(req, resp);
        }
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.getRequestDispatcher("index.jsp").forward(req, resp);
    }
}
