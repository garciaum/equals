package com.app.querySql;

import com.app.dbconfig.MysqlConnect;
import com.app.model.DetalheModel;
import com.app.model.HeaderModel;
import com.app.model.LayoutArquivo;
import org.jetbrains.annotations.Nullable;

import java.sql.*;
import java.util.ArrayList;

public class PesquisaBanco {
    public static LayoutArquivo ObterDados(@Nullable Long numeroEstabelecimento, @Nullable Date dataInicio,
                                           @Nullable Date dataFim, @Nullable String codNsu, @Nullable String cartaoBin, @Nullable String cartaoHolder) {
        LayoutArquivo layoutArquivo = new LayoutArquivo();
        layoutArquivo.Detalhes = new ArrayList<>();
        MysqlConnect mysqlConnect = new MysqlConnect();
        try {
            Connection connection = mysqlConnect.connect();
            String sql = "SELECT header.Dt_InicioPeriodo as Dt_InicioPeriodo, header.Dt_FimPeriodo as Dt_FimPeriodo," +
                    "header.Nu_Estabelecimento as Nu_Estabelecimento, header.No_Adquirente as No_Adquirente," +
                    "header.Dt_Processamento as Dt_Processamento, detalhe.Dt_Transacao as Dt_Transacao," +
                    "detalhe.Dt_Evento as Dt_Evento, detalhe.Hr_Evento as Hr_Evento, detalhe.Vl_Transacao as Vl_Transacao," +
                    "detalhe.Tp_Pagamento as Tp_Pagamento, detalhe.Qt_Parcelas as Qt_Parcelas," +
                    "detalhe.Dt_PrevistaPagamento as Dt_PrevistaPagamento, detalhe.Tx_Parcelamento as Tx_Parcelamento," +
                    "detalhe.Tx_Intermediacao as Tx_Intermediacao, detalhe.Tr_Intermediacao as Tr_Intermediacao," +
                    "detalhe.Vl_LiquidoTransacao as Vl_LiquidoTransacao, detalhe.No_Bandeira as No_Bandeira, " +
                    "detalhe.Tp_Evento as Tp_Evento, detalhe.Tp_Transacao as Tp_Transacao, " +
                    "detalhe.Cd_Transacao as Cd_Transacao, detalhe.Vl_ParcelaLiquida as Vl_ParcelaLiquida, " +
                    "detalhe.Cd_Pedido as Cd_Pedido, detalhe.Cd_Nsu as Cd_Nsu, " +
                    "detalhe.St_Pagamento as St_Pagamento, detalhe.Nu_CartaoBin as Nu_CartaoBin," +
                    "detalhe.Nu_CartaoHolder as Nu_CartaoHolder " +
                    "FROM equalsdb.tb_detalhe as detalhe " +
                    "JOIN equalsdb.tb_header header ON header.IdHeader = detalhe.Id_Header " +
                    "JOIN equalsdb.tb_trailer trailer ON trailer.Id_Trailer = detalhe.Id_Trailer";

            boolean temDadoPesquisado = numeroEstabelecimento != null || dataInicio != null || dataFim != null || (codNsu != null && !codNsu.isEmpty()) ||
                    (cartaoBin != null && !cartaoBin.isEmpty()) || (cartaoHolder != null && !cartaoHolder.isEmpty());

            if (temDadoPesquisado) {
                sql = sql + " WHERE ";
                boolean temClausula = false;
                if (numeroEstabelecimento != null) {
                    sql = sql + "detalhe.Nu_Estabelecimento = ? ";
                    temClausula = true;
                }

                if (dataInicio != null) {
                    if (temClausula)
                        sql = sql + " AND detalhe.Dt_Transacao >= ? ";
                    else
                        sql = sql + " detalhe.Dt_Transacao >= ? ";
                    temClausula = true;
                }
                if (dataFim != null) {
                    if (temClausula)
                        sql = sql + " AND detalhe.Dt_Transacao <= ?";
                    else
                        sql = sql + " detalhe.Dt_Transacao <= ?";
                    temClausula = true;
                }
                if (codNsu != null && !codNsu.isEmpty()) {
                    if (temClausula)
                        sql = sql + " AND detalhe.Cd_Nsu LIKE ?";
                    else
                        sql = sql + " detalhe.Cd_Nsu LIKE ?";
                    temClausula = true;
                }
                if (cartaoBin != null && !cartaoBin.isEmpty()) {
                    if (temClausula)
                        sql = sql + " AND detalhe.Nu_CartaoBin LIKE ?";
                    else
                        sql = sql + " detalhe.Nu_CartaoBin LIKE ?";
                    temClausula = true;
                }
                if (cartaoHolder != null && !cartaoHolder.isEmpty()) {
                    if (temClausula)
                        sql = sql + " AND detalhe.Nu_CartaoHolder LIKE ?";
                    else
                        sql = sql + " detalhe.Nu_CartaoHolder LIKE ?";
                }
            }
            PreparedStatement statement = connection.prepareStatement(sql);
            if (temDadoPesquisado) {
                Integer count = 0;
                if (numeroEstabelecimento != null) {
                    count = count + 1;
                    statement.setLong(count, numeroEstabelecimento);
                }
                if (dataInicio != null) {
                    count = count + 1;
                    statement.setDate(count, dataInicio);
                }
                if (dataFim != null){
                    count = count + 1;
                    statement.setDate(count, dataFim);
                }
                if (codNsu != null && !codNsu.isEmpty()) {
                    count = count + 1;
                    statement.setString(count, "%" + codNsu + "%");
                }
                if (cartaoBin != null && !cartaoBin.isEmpty()) {
                    count = count + 1;
                    statement.setString(count, "%" + cartaoBin + "%");
                }
                if (cartaoHolder != null && !cartaoHolder.isEmpty()) {
                    count = count + 1;
                    statement.setString(count, "%" + cartaoHolder + "%");
                }
            }
            ResultSet result = statement.executeQuery();
            if (result.next()) {
                if (layoutArquivo.Header == null) {
                    layoutArquivo.Header = new HeaderModel();
                    layoutArquivo.Header.setDataInicioPeriodo(result.getDate("Dt_InicioPeriodo"));
                    layoutArquivo.Header.setDataFimPeriodo(result.getDate("Dt_FimPeriodo"));
                    layoutArquivo.Header.setNumeroEstabelecimento(result.getLong("Nu_Estabelecimento"));
                    layoutArquivo.Header.setNomeAdquirente(result.getString("No_Adquirente"));
                    layoutArquivo.Header.setDataProcessamento(result.getDate("Dt_Processamento"));
                }
                while (result.next()) {
                    DetalheModel detalheModel = new DetalheModel();
                    detalheModel.setDataTransacao(result.getDate("Dt_Transacao"));
                    detalheModel.setDataEvento(result.getDate("Dt_Evento"));
                    detalheModel.setHoraEvento(result.getTime("Hr_Evento"));
                    detalheModel.setValorTransacao(result.getDouble("Vl_Transacao"));
                    detalheModel.setPagamento(result.getString("Tp_Pagamento"));
                    detalheModel.setQtdParcelas(result.getInt("Qt_Parcelas"));
                    detalheModel.setDataPrevistaPagamento(result.getDate("Dt_PrevistaPagamento"));
                    detalheModel.setTaxaParcelamento(result.getDouble("Tx_Parcelamento"));
                    detalheModel.setTaxaIntermediacao(result.getDouble("Tx_Intermediacao"));
                    detalheModel.setTarifaIntermediacao(result.getDouble("Tr_Intermediacao"));
                    detalheModel.setValorLiquidoTransacao(result.getDouble("Vl_LiquidoTransacao"));
                    detalheModel.setNomeInstituicaoBandeira(result.getString("No_Bandeira"));
                    detalheModel.setStatusPagamento(result.getInt("St_Pagamento"));
                    detalheModel.setTipoEvento(result.getInt("Tp_Evento"));
                    detalheModel.setTipoTransacao(result.getInt("Tp_Transacao"));
                    detalheModel.setCodigoPedido(result.getString("Cd_Pedido"));
                    detalheModel.setValorParcelaLiquido(result.getDouble("Vl_ParcelaLiquida"));
                    detalheModel.setCodigoTransacao(result.getString("Cd_Transacao"));
                    detalheModel.setCodigoNsu((result.getString("Cd_Nsu")));
                    detalheModel.setCartaoBin(result.getString("Nu_CartaoBin"));
                    detalheModel.setCartaoHolder(result.getString("Nu_CartaoHolder"));
                    layoutArquivo.Detalhes.add(detalheModel);
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return layoutArquivo;
    }
}
