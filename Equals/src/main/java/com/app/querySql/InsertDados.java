package com.app.querySql;

import com.app.model.DetalheModel;
import com.app.model.HeaderModel;
import com.app.model.TrailerModel;

import java.sql.*;
import java.util.List;

public class InsertDados {
    public static Integer salvarTrailer(Connection connection, TrailerModel trailer) {
        String sql = "INSERT INTO tb_trailer (" +
                "To_Registros)" +
                "VALUES (?)";
        Integer id = 0;
        try {
            PreparedStatement statement = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
            statement.setLong(1, trailer.getTotalRegistros());
            int rows = statement.executeUpdate();
            if (rows == 0) {
                throw new SQLException("Creating header failed, no rows affected.");
            }
            try (ResultSet generatedKeys = statement.getGeneratedKeys()) {
                if (generatedKeys.next()) {
                    id = generatedKeys.getInt(1);
                } else {
                    throw new SQLException("Creating header failed, no ID obtained.");
                }
            }
            statement.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return id;
    }

    public static void salvarDetalhe(Connection connection, List<DetalheModel> detalhes, Integer idHeader, Integer idTrailer) {
        String sql = "INSERT INTO tb_detalhe (" +
                "Id_Header," +
                "Id_Trailer," +
                "Nu_Estabelecimento," +
                "Dt_Transacao," +
                "Dt_Evento," +
                "Hr_Evento," +
                "Vl_Transacao," +
                "Tp_Pagamento," +
                "Qt_Parcelas," +
                "Dt_PrevistaPagamento," +
                "Tx_Parcelamento," +
                "Tx_Intermediacao," +
                "Tr_Intermediacao," +
                "Vl_LiquidoTransacao," +
                "No_Bandeira," +
                "Cd_Transacao," +
                "Vl_ParcelaLiquida," +
                "Cd_Pedido," +
                "Tp_Transacao," +
                "Tp_Evento," +
                "St_Pagamento," +
                "Nu_CartaoBin," +
                "Nu_CartaoHolder," +
                "Cd_Nsu)" +
                "VALUES (" +
                "?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
        try {
            PreparedStatement statement = connection.prepareStatement(sql);
            for (DetalheModel detalhe: detalhes) {
                statement.setInt(1, idHeader);
                statement.setInt(2, idTrailer);
                statement.setLong(3, detalhe.getCodigoEstabelecimento());
                statement.setDate(4, detalhe.getDataTransacao());
                statement.setDate(5, detalhe.getDataEvento());
                statement.setTime(6, detalhe.getHoraEvento());
                statement.setDouble(7, detalhe.getValorTransacao());
                statement.setString(8, detalhe.getPagamento());
                statement.setDouble(9, detalhe.getQtdParcelas());
                statement.setDate(10, detalhe.getDataPrevistaPagamento());
                statement.setDouble(11, detalhe.getTaxaParcelamento());
                statement.setDouble(12, detalhe.getTaxaIntermediacao());
                statement.setDouble(13, detalhe.getTarifaIntermediacao());
                statement.setDouble(14, detalhe.getValorLiquidoTransacao());
                statement.setString(15, detalhe.getNomeInstituicaoBandeira());
                statement.setString(16, detalhe.getCodigoTransacao());
                statement.setDouble(17, detalhe.getValorParcelaLiquido());
                statement.setString(18, detalhe.getCodigoPedido());
                statement.setInt(19, detalhe.getTipoTransacao());
                statement.setInt(20, detalhe.getTipoEvento());
                statement.setInt(21, detalhe.getStatusPagamento());
                statement.setString(22, detalhe.getCartaoBin());
                statement.setString(23, detalhe.getCartaoHolder());
                statement.setString(24, detalhe.getCodigoNsu());
                int rows = statement.executeUpdate();
                if (rows == 0) {
                    throw new SQLException("Creating detalhe failed, no rows affected.");
                }
            }
            statement.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static Integer salvarHeader(Connection connection, HeaderModel header) throws SQLException {
        String sql = "INSERT INTO tb_header (" +
                "Dt_InicioPeriodo," +
                "Dt_FimPeriodo," +
                "Nu_Estabelecimento," +
                "Dt_Processamento," +
                "Nu_Sequencial," +
                "No_Adquirente," +
                "Tp_Extrato)" +
                "VALUES (" +
                "?, ?, ?, ?, ?, ?, ?)";
        Integer id = 0;
        try {
            PreparedStatement statement = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
            statement.setDate(1, header.getDataInicioPeriodo());
            statement.setDate(2, header.getDataFimPeriodo());
            statement.setLong(3, header.getNumeroEstabelecimento());
            statement.setDate(4, header.getDataProcessamento());
            statement.setLong(5, header.getSequencial());
            statement.setString(6, header.getNomeAdquirente());
            statement.setLong(7, header.getTipoExtrato());
            int rows = statement.executeUpdate();
            if (rows == 0) {
                throw new SQLException("Creating header failed, no rows affected.");
            }
            try (ResultSet generatedKeys = statement.getGeneratedKeys()) {
                if (generatedKeys.next()) {
                    id = generatedKeys.getInt(1);
                } else {
                    throw new SQLException("Creating header failed, no ID obtained.");
                }
            }
            statement.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return id;
    }
}
