package com.app.utils;

import java.sql.Time;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.sql.Date;

public class Extensions {
    public static Date stringParaData(String stringData) {
        Date sqlDate = null;
        try {
            if (!stringData.isEmpty()) {
                java.util.Date data = new SimpleDateFormat("yyyyMMdd").parse(stringData);
                sqlDate = new java.sql.Date(data.getTime());
            }
        } catch (ParseException pe) {
            System.out.println(pe.getMessage());
        }
        return sqlDate;
    }
    public static Long stringParaLong(String stringNumero) {
        Long numero = null;
        try {
            if (!stringNumero.isEmpty())
                numero = Long.parseLong(stringNumero);

        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        return numero;
    }
    public static Integer stringParaInteger(String stringNumero) {
        Integer numero = null;
        try {
            if (!stringNumero.isEmpty())
                numero = Integer.parseInt(stringNumero);

        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        return numero;
    }
    public static Time stringParaHora(String stringHora) {
        Time hora = null;
        try {
            java.util.Date date = new SimpleDateFormat("hhmmss").parse(stringHora);
            hora = new Time(date.getTime());

        } catch(Exception e) {
            System.out.println(e.getMessage());
        }
        return hora;
    }
    public static Double stringParaDouble(String stringDouble, Integer precisao){
        Double valor = null;
        try {
            if (!stringDouble.isEmpty()) {
                StringBuilder sb = new StringBuilder(stringDouble);
                if (precisao != null && precisao > 0) {
                    Integer posicao = stringDouble.length() - precisao;
                    sb.insert(posicao, ".");
                }
                valor = Double.valueOf(sb.toString());
            }
        } catch (Exception e){
            System.out.println(e.getMessage());
        }
        return valor;
    }
}
