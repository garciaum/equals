<%@ page import ="java.util.*" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
    <link rel="stylesheet" href="./css/bootstrap.min.css">
    <script src="./js/bootstrap.min.js"></script>
</head>
    <body>
        <nav class="navbar navbar-dark bg-dark">
          <span class="navbar-brand mb-0 h1">Equals</span>
        </nav>
        <div class="container">
            <h3 class="text-center">Bem-Vindo ao Equals!</h3>
            <form method="post" action="EqualsPesquisa">
                <h3>Filtrar Resultados</h5>
                <div class="form-row">
                    <div class="form-group col">
                        <label for="numeroEstabelecimento">Código do Estabelecimento</label>
                        <input type="Text" title="Código do Estabelecimento" class="form-control" name="numeroEstabelecimento">
                    </div>
                    <div class="form-group col">
                        <label for="dataInicio">Data Inícial da Transação</label>
                        <input type="Date" title="Data Inícial do Período da Transação" class="form-control" name="dataInicio">
                    </div>
                    <div class="form-group col">
                        <label for="dataFim">Data Final da Transação</label>
                        <input type="Date" title="Data Final do Período da Transação" class="form-control" name="dataFim">
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-group col">
                        <label for="codigoNsu">Número do NSU</label>
                        <input type="Text" title="Número do NSU" class="form-control" name="codigoNsu">
                    </div>
                    <div class="form-group col">
                        <label for="cartaoBin">Número Cartão Bin</label>
                        <input type="Text" title="6 primeiros dígitos do cartão" class="form-control" name="cartaoBin">
                    </div>
                    <div class="form-group col">
                        <label for="cartaoHolder">Número Cartão Holder</label>
                        <input type="Text" title="4 últimos dígitos do cartão" class="form-control" name="cartaoHolder">
                    </div>
                </div>
                <button type="submit" class="text-center btn btn-primary">Pesquisar</button>
            </form>
            <br>
            <div style="margin-top: -62px; float: right;">
                <form method="post" action="EqualsAdquirente">
                    <button type="submit" title="Importar um novo arquivo localizado na pasta Home (user.home) do computador" class="text-center btn btn-info">Importar</button>
                </form>
            </div>
        </div>
        <br>
        <div class="container-fluid">
            <div class="table-responsive">
                <table class="table table-striped table-bordered table-hover table-sm" style="overflow-x:scroll">
                    <thead class="thead-light">
                        <tr>
                            <th>Código Estab.</th>
                            <th>Adquirente</th>
                            <th>Data Processamento</th>
                            <th>Data Transação</th>
                            <th>Data Evento</th>
                            <th>Hora Evento</th>
                            <th>Código Transação</th>
                            <th>Número do Cartão</th>
                            <th>NSU</th>
                            <th>Valor Transação (R$)</th>
                            <th>Valor Parcela/Líquido (R$)</th>
                            <th>Tipo Pagamento</th>
                            <th>Qtd. Parcelas</th>
                            <th>Data Prevista Pagamento</th>
                            <th>Taxa Parcelamento</th>
                            <th>Taxa Intermediação</th>
                            <th>Valor Líquido Transação (R$)</th>
                            <th>Status do Pagamento</th>
                            <th>Nome da Instituição/Bandeira</th>
                        </tr>
                    </thead>
                    <tbody>
                        <c:choose>
                            <c:when test="${not empty Detalhes}">
                                <c:forEach items="${Detalhes}" var="detalhe" varStatus="count">
                                    <tr id="${count.index}">
                                        <td>${Header.getNumeroEstabelecimento()}</td>
                                        <td>${Header.getNomeAdquirente()}</td>
                                        <td>${Header.getDataProcessamento()}</td>
                                        <td>${detalhe.getDataTransacao()}</td>
                                        <td>${detalhe.getDataEvento()}</td>
                                        <td>${detalhe.getHoraEvento()}</td>
                                        <td>${detalhe.getCodigoTransacao()}</td>
                                        <td>${detalhe.getCartaoBin()}******${detalhe.getCartaoHolder()}</td>
                                        <td>${detalhe.getCodigoNsu()}</td>
                                        <td>${detalhe.getValorTransacao()}</td>
                                        <td>${detalhe.getValorParcelaLiquido()}</td>
                                        <td>${detalhe.getPagamento()}</td>
                                        <td>${detalhe.getQtdParcelas()}</td>
                                        <td>${detalhe.getDataPrevistaPagamento()}</td>
                                        <td>${detalhe.getTaxaParcelamento()}</td>
                                        <td>${detalhe.getTaxaIntermediacao()}</td>
                                        <td>${detalhe.getValorLiquidoTransacao()}</td>
                                        <td>${detalhe.getStatusPagamento()}</td>
                                        <td>${detalhe.getNomeInstituicaoBandeira()}</td>
                                    </tr>
                                </c:forEach>
                            </c:when>
                            <c:otherwise>
                                <tr>
                                    <td colspan="19" class="text-center">Nâo há dados disponíveis</td>
                                </tr>
                            </c:otherwise>
                        </c:choose>
                    </tbody>
                </table>
            </div>
        </div>
    </body>
</html>